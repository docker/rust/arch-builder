FROM archlinux/base
LABEL maintainer="elois <elois@ifee.fr>,tuxmain <tuxmain@zettascript.org>"
LABEL version="1.37"
LABEL description="To compile and package rust program for arch linux on x64 architecture"

# update & upgrade
RUN pacman -Syu --noconfirm

# install gcc
RUN pacman -S --noconfirm gcc grep sudo git fakeroot

# create user
RUN useradd builduser --create-home
RUN echo "builduser ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
RUN chown builduser /home/builduser

USER builduser

# install rustup
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y
ENV PATH /home/builduser/.cargo/bin:$PATH
RUN rustup install stable
RUN rustup default stable

# install cargo-arch
RUN cargo install --git https://github.com/ZettaScript/cargo-arch